Super Simple Unit testing
-------------------------

Being bored with existing unit testing and finding them too complex and complicated,
I wanted to create something easy, stateless and something that would "just work"™.

Welcome SSTEST library.

You just test an expression, it either fails or succeeds. No types, not switches, just
an expression. And colors, red ones are bad, green ones are good, yellow ones are to do.

# Example:

See [tests/test_example.c](tests/test_example.c).

# Written by

Written by Kamil Cukrowski 2022
Licensed jointly under Beerware License and MIT License.


